FROM jenkins/jenkins:lts

USER root

# necessary steps to run docker inside jenkins container
RUN apt-get update && apt-get install -y libltdl7 && rm -rf /var/lib/apt/lists/*

ARG docker_group_gid
RUN groupadd -fr -g ${docker_group_gid} docker
RUN usermod -aG docker jenkins

# drop back to the regular jenkins user - good practice
USER jenkins
